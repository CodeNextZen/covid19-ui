export const environment = {
  production: true,
  apiUrl: 'https://coronavirus-monitor.p.rapidapi.com/coronavirus/',
  flagUrl: 'https://restcountries.eu/rest/v2/name/',
  flagUrlBasedonCode: 'https://restcountries.eu/rest/v2/alpha/',
  hostUrl: 'https://www.codenextzen.com/covid19/'
};
