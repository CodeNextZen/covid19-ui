import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonErrorComponent } from './shared/components/common-error/common-error.component';
import { ErrorGuard } from './shared/error-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'error', canActivate: [ErrorGuard], component:  CommonErrorComponent},
  {
    path: 'dashboard',
    loadChildren: () => import('./main/main.module').then((m) => m.MainModule)
  },
  { path: '**', redirectTo: '/dashboard'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
