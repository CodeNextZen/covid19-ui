import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CovidService, PersistentService, HelperService } from '../../services';
import { NgxSpinnerService } from 'ngx-spinner';
import { generalContants, endpoints, flagData } from 'src/app/shared/constants';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { CountryStat, Stat, WorldStat } from 'src/app/shared/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy, AfterViewInit {

  subscription: Subscription = new Subscription();
  countries: string[];
  flagUrl: string;
  currentCountry: string;
  constructor(private covidService: CovidService,
              private persistentService: PersistentService,
              private helperService: HelperService,
              private spinner: NgxSpinnerService,
              private router: Router) {
    this.currentCountry = generalContants.defaultCountry;
    this.flagUrl = environment.hostUrl + endpoints.worldFlag;
  }
  ngAfterViewInit(): void {
    const subscription = this.covidService.getCasesForAllCountry().pipe(map((response: any) => {
      return this.helperService.mapCountryStat(response.body);
    })).subscribe((countriesStat: CountryStat) => {
      countriesStat.latestStatByCountry.sort((a: Stat, b: Stat) => {
        const param2 = +(a.totalCases.replace(/,/g, ''));
        const param1 = +(b.totalCases.replace(/,/g, ''));
        return (param1 > param2 ? 1 : ((param1 < param2) ? -1 : 0));
      });
      this.persistentService.updateAllCountriesStats(countriesStat);
    },
    (error) => {
      this.router.navigate(['/error'], { state: {active: true}});
    });
    this.subscription.add(subscription);
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  ngOnInit(): void {
    this.countries = [];
    for (const data of flagData) {
      this.countries.push(data.name);
    }
    const subscription = this.covidService.getWorldTotalStats().pipe(map((response: any) => {
      return this.helperService.mapWorldStat(response.body);
    })).subscribe((worldStat: WorldStat) => {
      this.persistentService.updateWorldTotalStats(worldStat);
      this.spinner.hide();
    },
    (error) => {
      this.router.navigate(['/error'], { state: {active: true}});
    });
    this.subscription.add(subscription);
  }
  onCountryChange(country: string): void {
    this.persistentService.updateCountryCasesStatSource(null);
    this.currentCountry = country;
    if (this.currentCountry === generalContants.defaultCountry) {
      this.flagUrl = environment.hostUrl + endpoints.worldFlag;
    } else {
      // const subscription = this.covidService.getFlagData(country).subscribe((FlagDataResponse: any) => {
      //   this.flagUrl = FlagDataResponse.body[0].flag;
      // });
      // this.subscription.add(subscription);
      this.flagUrl = flagData.find(d => d.name === country).flag;
      const subscription1 = this.covidService.getHistoryByCountry(country).pipe(map((response: any) => {
        return this.helperService.mapCountryStat(response.body);
      })).subscribe((data: CountryStat) => {
        this.persistentService.updateCountryCasesStatSource(data);
      });
      this.subscription.add(subscription1);
    }
  }
}
