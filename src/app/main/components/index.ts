export { CardComponent } from './card/card.component';
export { GlobeComponent } from './globe/globe.component';
export { MainComponent } from './main/main.component';
export { PieChartStatComponent } from './pie-chart-stat/pie-chart-stat.component';
