import { Component, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { generalContants, chartCOnfig } from 'src/app/shared/constants';
import { Subscription } from 'rxjs';
import { CountryStat, WorldStat, Stat } from 'src/app/shared/models';
import { ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { PersistentService } from '../../services';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  providers : [DecimalPipe]
})
export class CardComponent implements OnChanges, OnDestroy {
  @Input() cardType: string;
  @Input() currentCountry: string;
  subscription: Subscription = new Subscription();
  cardImage: string;
  cardLabel: string;
  cssClass = [];
  cases: string;
  chartOptions: ChartOptions;
  chartType: string;
  chartLabels: Label[];
  chartData: ChartDataSets[];
  toolTipLabel: string;
  isChartEnabled = false;
  constructor(private persistentService: PersistentService, private decPipe : DecimalPipe) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.isChartEnabled = false;
    if (changes.cardType) {
      this.cssClass = [];
      this.loadCard();
    }
    if (changes.currentCountry) {
      if (this.currentCountry === generalContants.defaultCountry) {
        const subscription = this.persistentService.worldTotalStatsDataSource.subscribe((data: WorldStat) => {
          if (data !== undefined && data !== null) {
            this.loadCardStat(data);
          }
        });
        this.subscription.add(subscription);
        const subscription1 = this.persistentService.allCountriesStatsDataSource.subscribe((data: CountryStat) => {
          if (data !== undefined && data !== null) {
            this.loadTopCuntriesChart(data.latestStatByCountry);
          }
        });
        this.subscription.add(subscription1);
      } else {
        const subscription = this.persistentService.allCountriesStatsDataSource.subscribe((data: CountryStat) => {
          if (data !== undefined && data !== null) {
            const modifiedData = data.latestStatByCountry.find(a => a.countryName === this.currentCountry);
            if (modifiedData.totalRecovered === 'N/A') {
              modifiedData.totalRecovered = (+(modifiedData.totalCases.replace(/,/g, '')) - (+modifiedData.totalDeaths.replace(/,/g, ''))
          - (+modifiedData.activeCases.replace(/,/g, ''))).toString();
            }
            this.loadCardStat(modifiedData);
          }
        });
        this.subscription.add(subscription);
        const subscription1 = this.persistentService.countryCasesStatSource.subscribe((data: CountryStat) => {
          if (data !== undefined && data !== null) {
            this.loadCountryHistoryChart(this.getCountryHistory(data.latestStatByCountry));
          }
        });
        this.subscription.add(subscription1);
      }
    }
  }
  loadCountryHistoryChart(countriesStat: Stat[]) {
    const label = [];
    const data = [];
    switch (this.cardType) {
      case 'totalCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          if (countriesStat[day] !== undefined && countriesStat[day] !== null) {
            label.push(countriesStat[day].recordDate.split(' ')[0]);
            data.push(countriesStat[day].totalCases.replace(/,/g, '').toString());
          }
        }
        this.toolTipLabel = generalContants.casesLabel;
        break;

      case 'deathCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          if (countriesStat[day] !== undefined && countriesStat[day] !== null) {
            label.push(countriesStat[day].recordDate.split(' ')[0]);
            data.push(countriesStat[day].totalDeaths.replace(/,/g, '').toString());
          }
        }
        this.toolTipLabel = generalContants.deathLabel;
        break;

      case 'activeCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          if (countriesStat[day] !== undefined && countriesStat[day] !== null) {
            label.push(countriesStat[day].recordDate.split(' ')[0]);
            data.push(countriesStat[day].activeCases.replace(/,/g, '').toString());
          }
        }
        this.toolTipLabel = generalContants.activeLabel;
        break;

      case 'recoveredCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          if (countriesStat[day] !== undefined && countriesStat[day] !== null) {
            label.push(countriesStat[day].recordDate.split(' ')[0]);
            data.push(countriesStat[day].totalRecovered.replace(/,/g, '').toString());
          }
        }
        this.toolTipLabel = generalContants.recoveredLabel;
        break;
    }
    this.chartData = JSON.parse(JSON.stringify(chartCOnfig.lineChartData));
    this.chartData[0].data = data;
    this.chartData[0].label = this.toolTipLabel;
    this.chartLabels = label;
    this.chartOptions = chartCOnfig.lineChartOptions;
    this.chartType = chartCOnfig.lineChartType;
    this.isChartEnabled = true;
  }
  loadTopCuntriesChart(countriesStat: Stat[]): void {
    const label = [];
    const data = [];
    switch (this.cardType) {
      case 'totalCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          label.push(countriesStat[day].countryName);
          data.push(countriesStat[day].totalCases.replace(/,/g, '').toString());
        }
        this.toolTipLabel = generalContants.casesLabel;
        break;

      case 'deathCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          label.push(countriesStat[day].countryName);
          data.push(countriesStat[day].totalDeaths.replace(/,/g, '').toString());
        }
        this.toolTipLabel = generalContants.deathLabel;
        break;

      case 'activeCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          label.push(countriesStat[day].countryName);
          data.push(countriesStat[day].activeCases.replace(/,/g, '').toString());
        }
        this.toolTipLabel = generalContants.activeLabel;
        break;

      case 'recoveredCases':
        for (let day = 0; day < generalContants.daysHistory; day++) {
          label.push(countriesStat[day].countryName);
          data.push(countriesStat[day].totalRecovered.replace(/,/g, '').toString());
        }
        this.toolTipLabel = generalContants.recoveredLabel;
        break;
    }
    this.chartData = JSON.parse(JSON.stringify(chartCOnfig.lineChartData));
    this.chartData[0].data = data;
    this.chartData[0].label = this.toolTipLabel;
    this.chartLabels = label;
    this.chartOptions = chartCOnfig.lineChartOptions;
    this.chartType = chartCOnfig.lineChartType;
    this.isChartEnabled = true;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  loadCardStat(data: Stat | WorldStat): void {
    switch (this.cardType) {
      case 'totalCases':
        this.cases = data.totalCases;
        break;

      case 'deathCases':
        this.cases = data.totalDeaths;
        break;

      case 'activeCases':
        this.cases = (+(data.totalCases.replace(/,/g, '')) - (+data.totalDeaths.replace(/,/g, ''))
          - (+data.totalRecovered.replace(/,/g, ''))).toString();
        this.cases =   this.decPipe.transform(this.cases);
        break;

      case 'recoveredCases':
        this.cases = data.totalRecovered;
        break;
    }
  }

  loadCard(): void {
    switch (this.cardType) {
      case 'totalCases':
        this.cssClass.push('overview-item');
        this.cssClass.push('overview-item--c1');
        this.cardImage = generalContants.totalCasesImage;
        this.cardLabel = generalContants.casesLabel;
        break;

      case 'deathCases':
        this.cssClass.push('overview-item');
        this.cssClass.push('overview-item--c3');
        this.cardImage = generalContants.deathCasesImage;
        this.cardLabel = generalContants.deathLabel;
        break;

      case 'activeCases':
        this.cssClass.push('overview-item');
        this.cssClass.push('overview-item--c4');
        this.cardImage = generalContants.activeCasesImage;
        this.cardLabel = generalContants.activeLabel;
        break;

      case 'recoveredCases':
        this.cssClass.push('overview-item');
        this.cssClass.push('overview-item--c2');
        this.cardImage = generalContants.recoveredCasesImage;
        this.cardLabel = generalContants.recoveredLabel;
        break;
    }
  }

  private getCountryHistory(data: Stat[]): Stat[] {
    const his = [];
    for (let day = generalContants.daysHistory - 1; day >= 0; day--) {
      const date = this.getNewDate(day);
      const temp = data.filter(x => x.recordDate.split(' ')[0] === date);
      his.push(temp[temp.length - 1]);
    }
    return his;
  }
  private getNewDate(num: number) {
    const date = new Date();
    date.setDate(date.getDate() - num);
    return date.toISOString().split('T')[0];
  }
}
