import { Component, OnInit, OnDestroy } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldLow from '@amcharts/amcharts4-geodata/worldLow';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Subscription } from 'rxjs';
import { CountryStat } from 'src/app/shared/models';
import { flagData } from 'src/app/shared/constants';
import { PersistentService } from '../../services';

am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-globe',
  templateUrl: './globe.component.html',
  styleUrls: ['./globe.component.css']
})
export class GlobeComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  private statData = [];
  private flagUrls = [];
  private animation: am4core.Animation;
  private animationInterval;
  isMobileDevice: boolean;
  private chart: any;

  constructor(private persistentService: PersistentService,
              private deviceService: DeviceDetectorService) {
    this.isMobileDevice = this.deviceService.isMobile();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    clearInterval(this.animationInterval);
    this.animationInterval = null;
  }

  ngOnInit() {
    this.loadStatData();
    // load flagurls data
    for (const data of flagData) {
      this.flagUrls[data.alpha2Code] = data.flag;
    }
  }

  loadStatData() {
    // load countryStat data
    const respdata = [];
    const subscription = this.persistentService.allCountriesStatsDataSource.subscribe((countryStat: CountryStat) => {
      if (countryStat !== undefined && countryStat !== null) {
        for (const cdata of countryStat.latestStatByCountry) {
          respdata[cdata.countryName] = {
            cases: cdata.totalCases,
            deaths: cdata.totalDeaths,
            total_recovered: cdata.totalRecovered,
            active_cases: cdata.activeCases,
            new_deaths: cdata.newDeaths,
            new_cases: cdata.newCases
          };
        }
        this.statData = respdata;
        this.createGlobalChart();
      }
    });
    this.subscription.add(subscription);
  }

  createGlobalChart() {

    this.chart = am4core.create('globechartdiv', am4maps.MapChart);
    this.chart.height = am4core.percent(100);
    this.chart.zoomEasing = am4core.ease.sinOut;
    this.chart.panBehavior = 'move';
    this.chart.responsive.enabled = true;

    // Set map definition
    this.chart.geodata = am4geodata_worldLow;

    // Set projection
    this.chart.projection = new am4maps.projections.Orthographic();
    this.chart.panBehavior = 'rotateLong';
    this.chart.deltaLatitude = -20;
    this.chart.deltaLongitude = -70;
    this.chart.background.clickable = false;

    // limits vertical rotation
    this.chart.adapter.add('deltaLatitude', (delatLatitude) => {
      return am4core.math.fitToRange(delatLatitude, -70, 70);
    });

    // Create map polygon series
    const polygonSeries = this.chart.series.push(new am4maps.MapPolygonSeries());

    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;
    polygonSeries.calculateVisualCenter = true;
    polygonSeries.mapPolygons.template.tooltipPosition = 'fixed';
    this.chart.seriesContainer.draggable = false;
    this.chart.seriesContainer.resizable = true;
    this.chart.maxZoomLevel = 1.5;
    this.chart.centerMapOnZoomOut = true;
    this.chart.seriesContainer.events.disableType('doublehit');
    this.chart.chartContainer.background.events.disableType('doublehit');

    // Configure series
    const polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.nonScalingStroke = true;
    polygonTemplate.propertyFields.id = 'id';

    polygonSeries.mapPolygons.template.events.on('over', (ev) => {
      const countryName = flagData.find(d => d.alpha2Code === ev.target.id).name;
      if (countryName !== undefined && this.statData[countryName] !== undefined) {
        ev.target.contentAlign = 'center';
        ev.target.contentHeight = 250;
        ev.target.contentValign = 'middle';
        ev.target.contentWidth = 100;
        ev.target.tooltip.setBounds({ x: 0, y: 0, width: 200000, height: 200000 });
        ev.target.tooltipHTML = `<div style="max-width: 120px; word-wrap: break-word; width: 100%">
          <center><strong>{name}</strong><br/><img src=" ` + this.flagUrls[ev.target.id] + ` "></center></div>
          <div style="width: 100%"><center><hr style="margin-top: 8px; margin-bottom: 5px;" />
          <table style="color: #36454f;font-size: 12px;">
          <tr>
            <td><center>` + this.statData[countryName].cases + ` Cases</center></td>
          </tr>
          <tr>
            <td><center>` + this.statData[countryName].active_cases + ` Active</center></td>
          </tr>
          <tr>
            <td><center>` + this.statData[countryName].deaths + ` Deaths</center></td>
          </tr>
          <tr>
            <td><center>` + this.statData[countryName].total_recovered + ` Recovered</center></td>
          </tr>
          <tr>
            <td><center><strong>TODAY<strong></center></td>
          </tr>
          <tr>
            <td><center>+` + this.statData[countryName].new_cases + ` Cases</center></td>
          </tr>
          <tr>
            <td><center>+` + this.statData[countryName].new_deaths + ` Deaths</center></td>
          </tr>
          </table><hr style="margin-top: 3px; margin-bottom: 4px;"/></center></div>`;
      }
    });

    polygonTemplate.fill = am4core.color('#ffcccb');
    polygonTemplate.stroke = am4core.color('#f70024');
    polygonTemplate.strokeWidth = 0.5;

    const graticuleSeries = this.chart.series.push(new am4maps.GraticuleSeries());
    graticuleSeries.mapLines.template.line.stroke = am4core.color('#000000');
    graticuleSeries.mapLines.template.line.strokeOpacity = 0.08;
    graticuleSeries.fitExtent = false;

    this.chart.backgroundSeries.mapPolygons.template.polygon.fillOpacity = 0.1;
    this.chart.backgroundSeries.mapPolygons.template.polygon.fill = am4core.color('#f70024');

    // Create hover state and set alternative fill color
    const hs = polygonTemplate.states.create('hover');
    hs.properties.fill = am4core.color('#FF0000');

    setTimeout(() => {
      this.animation = this.chart.animate({ property: 'deltaLongitude', to: 100000 }, 20000000);
    }, 3000);

    polygonSeries.mapPolygons.template.events.on('over', () => {
      if (this.animation) {
        this.animation.pause();
        clearInterval(this.animationInterval);
        this.animationInterval = null;
        this.animationInterval = setInterval(() => {
          if (this.animation && !this.chart.isInTransition()) {
            this.animation.resume();
          }
        }, 20000);
      }
    });

    this.chart.events.on('down', () => {
      if (this.animation) {
        this.animation.stop();
      }
    });
  }

  onGlobeReset() {
    this.chart.goHome();
  }

}
