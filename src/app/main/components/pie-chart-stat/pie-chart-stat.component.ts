import { Component, OnInit, Input, NgZone, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am4core from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { generalContants } from 'src/app/shared/constants';
import { WorldStat, CountryStat, Stat } from 'src/app/shared/models';
import { Subscription } from 'rxjs';
import { PersistentService } from '../../services';

am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-pie-chart-stat',
  templateUrl: './pie-chart-stat.component.html',
  styleUrls: ['./pie-chart-stat.component.css']
})
export class PieChartStatComponent implements OnChanges, OnDestroy {
  // amchart data
  private chart: am4charts.PieChart3D;
  @Input() currentCountry: string;
  isMobileDevice: boolean;
  subscription: Subscription = new Subscription();
  constructor(private zone: NgZone, private deviceService: DeviceDetectorService, private persistentService: PersistentService) {
    this.isMobileDevice = this.deviceService.isMobile();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
    this.chart = null;
    this.createPieChart();
  }
  createPieChart() {
    if (this.currentCountry === generalContants.defaultCountry) {
      const subscription = this.persistentService.worldTotalStatsDataSource.subscribe((worldStat: WorldStat) => {
        if (worldStat !== undefined && worldStat !== null) {
          this.loadChart(worldStat);
        }
      });
      this.subscription.add(subscription);
    } else {
      const subscription = this.persistentService.allCountriesStatsDataSource.subscribe((data: CountryStat) => {
        if (data !== undefined && data !== null) {
          const modifiedData = data.latestStatByCountry.find(a => a.countryName === this.currentCountry);
          this.loadChart(modifiedData);
        }
      });
      this.subscription.add(subscription);
    }
  }
  loadChart(data: WorldStat | Stat) {
    this.zone.runOutsideAngular(() => {
      const chart = am4core.create('piechartdiv', am4charts.PieChart3D);
      chart.hiddenState.properties.opacity = 0;
      chart.responsive.enabled = true;
      chart.legend = new am4charts.Legend();
      chart.data = [
        {
          stat: generalContants.activeLabel,
          count: (+(data.totalCases.replace(/,/g, '')) - (+data.totalDeaths.replace(/,/g, ''))
            - (+data.totalRecovered.replace(/,/g, ''))).toString()
        },
        {
          stat: generalContants.deathLabel,
          count: data.totalDeaths
        },
        {
          stat: generalContants.recoveredLabel,
          count: data.totalRecovered
        }
      ];
      chart.innerRadius = 100;
      const series = chart.series.push(new am4charts.PieSeries3D());
      series.colors.list = [
        am4core.color('#FFA500'),
        am4core.color('#FF0000'),
        am4core.color('#388E3C'),
      ];
      series.dataFields.value = 'count';
      series.dataFields.category = 'stat';
      if (this.isMobileDevice) {
        chart.radius = 90;
        chart.innerRadius = 70;
        series.labels.template.text = '{value.percent.formatNumber(\'#.0\')}%';
      }
      this.chart = chart;
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
