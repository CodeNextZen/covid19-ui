import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


import { CardComponent, PieChartStatComponent, GlobeComponent, MainComponent } from './components';
import { MainRoutingModule } from './main-routing.module';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    CardComponent,
    PieChartStatComponent,
    GlobeComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    ChartsModule,
    NgbModule,
    DeviceDetectorModule.forRoot(),
    FontAwesomeModule
  ],
  providers: []
})
export class MainModule { }
