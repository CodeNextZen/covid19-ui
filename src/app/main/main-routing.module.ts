import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components';
import { NgModule } from '@angular/core';

export const mainRoutes: Routes = [
    { path: '', component: MainComponent }
];

@NgModule({
    imports: [RouterModule.forChild(mainRoutes)],
    exports: [RouterModule]
  })
  export class MainRoutingModule { }
