import { Injectable } from '@angular/core';
import { CountryStat, WorldStat, Stat } from 'src/app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }

  public mapStatModel(data: any): Stat {
    const stat = new Stat();
    stat.id = data.id || null;
    stat.countryName = data.country_name;
    stat.totalCases = data.total_cases || data.cases || '0';
    stat.newCases = data.new_cases;
    stat.activeCases = data.active_cases || '0';
    stat.totalDeaths = data.total_deaths || data.deaths || '0';
    stat.newDeaths = data.new_deaths;
    stat.totalRecovered = data.total_recovered || '0';
    stat.seriousCritical = data.serious_critical;
    stat.region = data.region;
    stat.totalCasesPer1m = data.total_cases_per1m || data.total_cases_per_1m_population;
    stat.recordDate = data.record_date || null;
    return stat;
  }

  public mapWorldStat(data: any): WorldStat {
    const worldStat = new WorldStat();
    worldStat.totalCases = data.total_cases;
    worldStat.totalDeaths = data.total_deaths;
    worldStat.totalRecovered = data.total_recovered;
    worldStat.newCases = data.new_cases;
    worldStat.newDeaths = data.new_deaths;
    worldStat.statisticTakenAt = data.statistic_taken_at;
    return worldStat;
  }

  public mapCountryStat(data: any): CountryStat {
    const countryStat = new CountryStat();
    countryStat.latestStatByCountry = [];
    countryStat.country = data.country || '0';
    (data.latest_stat_by_country || data.stat_by_country || data.countries_stat).forEach(element => {
      countryStat.latestStatByCountry.push(this.mapStatModel(element));
    });
    return countryStat;
  }

  public getLineChartDataConfig() {
    return [
      {
        data: null,
        type: 'line',
        label: 'Cases',
        backgroundColor: 'transparent',
        borderColor: 'rgba(255,255,255,.55)',
        pointBackgroundColor: 'rgba(255,255,255,.1)',
        pointBorderColor: 'rgba(255,255,255,.5)',
        pointHoverBackgroundColor: 'rgba(255,255,255,.1)',
        pointHoverBorderColor: 'rgba(255,255,255,.1)'
      }
    ];
  }
}
