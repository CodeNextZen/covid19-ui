import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WorldStat, CountryStat } from 'src/app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class PersistentService {

  constructor() { }
  worldTotalStatsDataSource: BehaviorSubject<WorldStat> = new BehaviorSubject<WorldStat>(null);
  allCountriesStatsDataSource: BehaviorSubject<CountryStat> = new BehaviorSubject<CountryStat>(null);
  countryCasesStatSource: BehaviorSubject<CountryStat> = new BehaviorSubject<CountryStat>(null);
  updateWorldTotalStats(worldStat: WorldStat) {
    this.worldTotalStatsDataSource.next(worldStat);
  }
  updateAllCountriesStats(countriesStat: CountryStat) {
    this.allCountriesStatsDataSource.next(countriesStat);
  }
  updateCountryCasesStatSource(countryStat: CountryStat) {
    this.countryCasesStatSource.next(countryStat);
  }
}
