import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { endpoints, httpOptions, httpResponseOptions } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class CovidService {

  constructor(private http: HttpClient) { }

  getWorldTotalStats() {
    return this.http.get<any>(environment.apiUrl + endpoints.worldstat , httpOptions);
  }
  getCasesForAllCountry() {
    return this.http.get<any>(environment.apiUrl + endpoints.casesByCountry , httpOptions);
  }
  getHistoryByCountry(country: string) {
    return this.http.get<any>(environment.apiUrl + endpoints.casesByParticularCountry + country, httpOptions);
  }
}
