export class WorldStat {
    totalCases: string;
    totalDeaths: string;
    totalRecovered: string;
    newCases: string;
    newDeaths: string;
    statisticTakenAt: string;
}
