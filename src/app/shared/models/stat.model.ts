export class Stat {
    id?: string;
    countryName: string;
    totalCases: string;
    newCases: string;
    activeCases: string;
    totalDeaths: string;
    newDeaths: string;
    totalRecovered: string;
    seriousCritical: string;
    region: string;
    totalCasesPer1m: string;
    recordDate?: string;
    CasesPerOneMillionPopulation?: string;
}
