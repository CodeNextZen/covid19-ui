import { Stat } from './stat.model';

export class CountryStat {
    country?: string;
    latestStatByCountry: Stat[];
}
