import { InteractionMode } from 'chart.js';
import { HttpHeaders } from '@angular/common/http';

export const httpOptions = {
    headers: new HttpHeaders({
        'x-rapidapi-host': 'coronavirus-monitor.p.rapidapi.com',
        'x-rapidapi-key': '9d33889a0emsh1c70986a04743e2p107c66jsn839a3ab8d216'
    }),
    observe: 'response' as 'response'
};
export const httpResponseOptions = {
    observe: 'response' as 'response'
};

export const endpoints = {
    worldstat: 'worldstat.php',
    casesByParticularCountry: 'cases_by_particular_country.php?country=',
    casesByCountry: 'cases_by_country.php',
    randomMask: 'random_masks_usage_instructions.php',
    fullText: '?fullText=true',
    dateQuery: '&date=',
    worldFlag: 'assets/media/world.png'
};

export const generalContants = {
    defaultCountry: 'World',
    daysHistory: 5,
    deathLabel: 'Total Deaths',
    casesLabel: 'Total Cases',
    activeLabel: 'Active Cases',
    recoveredLabel: 'Recovered Cases',
    totalCasesImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAJKUlEQVR4nO2de3BU1R3HP+fu5gEqj/iAwGafQSWvIRaVh0AYBKyC6EwZlbF1bOv4R1umjpVpnenY9o/OqBWc2tZptY7aMthxRgGx7SgmGIgUNURDNgKGbDaJlYcmFRFCsvee/pEljbBhH/dsuNucz7/3nN/57X7zO7/fvb+zN6DRaDQajUaj0Wg0Go1GoxkBkQWbht9fuUBirUKw0DCER0qKhKDHsmQ3knqBsaWjY99OwMrC+jmNUkECgYplUvColHJW0oUNo8mSsXWd7R9tV+lDrqNGkJoatz969AmJWAswrXgiq26pZPGiGfhKJjN50nh6ek8S7ephR/3HbN62j08PHwdACp7sbC/7CbxsKvElx7EvSE2N2x899oqElQUFbh5cu5h71lxHfr57xCmnT8d4fuO7rH+qjv7+GAJe64iU3a5FAZddA34xfoNEfOfSoot48Zm7uWV5OS6X8bUxPb0nyc93YxiD+rvdBrOrS5g/J8j2ugOc6hu4anLRsYn/6T32T7v+5Dq2BAkEKpZJ+F1BgZsXn7mbWVWec8Zs/XsLq+9+jsJxecyuLvnateKpE7j2Gh+bt+3DNK05E4sua/ii97N2Oz7lOiPvK8kxpOBRJDy4dnFCMV5+tYmfPfIapik51TeQ0Mg1szz8+Ac1PLZhO4ZwPwbMxmb15fOXNyCYl8aUXdFIeIFNGwntpIuRfEhi/P7KBVLKWdOKJ3LPmuvOuT4wYPLwL7YRDFyW1NZ3v309xVMmIC2rOhCYeUOmPg0hkGnOOHfN9MVIbCdNMo4Qy7BuExJuX1mZMIHn5bl4ZdP3+XDfJ/z8V6+f11ZBgZvbVlby9LMNWLhuBeoz9QsgGgmf94sJhaquiFnWWyArktla/Of7U1qz7nt/TNG785NxhAg5+NewaMGMEcdUlhUjUizkauJ2BHJhpj6lQmlp9eWmZW6Pi7E/m2tlQsaCGIbwAvi9RUoc8cXtCEOUJBmaMaWl1ZfHzP63JFQC+4Vkcapz//XwS9ly62tkLIiUTAaYNHGcEkcmTxp3xq4ahc8ikRgdHeHDqc4/deSLbLh1DplvWYIeGLzHUEFP76m4Xfm5EoPDsCsGwLgpE1W7lZCMBbEs2QUQ7epR4ki0c9COadGlxGCcUKjqCrtiAMz59Z0q3RqRjAUhXgntqG9T4khd/UEABNJWhTWcM9WUXTFGk8yTOtYWgM3bmjl9OmbLib6+GFtfbwHAgq22jMU5q7S1LYbjk3ok8tEuYRhNnx4+zvMb37XlxHN/2c2nR44jhNjbFWndlcqckpKrpnkD5ff5/f7Cs68NblNm7aAYosVtuBbZjQzHJ3XAsmRsHcD6p+rY+0F3wkFTp17CJRcX4ClOnBTfb+riyd+/DYAU8iFIfpcdClWUGC53vYA/WeKiLR7P3HH/uxaPDEE5sF9IufTQoeajaX+6C4QdQehs/2g7kg39/THu++GmhKIsWXQlzXt+yrduP7dn9X5TF/f/6CUGBkyA9dFD4dpkawaD5d6YJXcgCAEIWGbkHX/V7/cXZhgZDQJ2pvSBRwFbggBEO8oeErC1p/ckd937Ak8/25A0p/T1xfjDMztZc+8L9PSeRMKWaKRsXbK1gsFyrympA4LAHixjHnBEwHLERVtjplk7GBmixW0YS1KJjGgkfENHJJz06cBolb2KWrirXT5/6+MIHgAonjKBVSsqWbxwBj5vEUWTBzuGHdEeduz8mM3bmjl85Mszk9cPinH+5lQoVFESM2VdPDIapWks7ezc1xsIVF1lYdYBxfGhSqopX6BcQvrPsqKRsK3vVGlP3Rsqn29Y4nGJnJtsrCFEoyXkulS3qeGR4RKFy9vbG4eyrNdbXiZcbAeOug3XMhU540IJYnvLGk7noXBDR6RlvoVcCOIJAe8ZhjgshBgwDHFYwHsgfyMFCyLtLdeqEAOgszPcellRoS8aKftGthL4aJW9dhpUIyG7Iq07UZAo49tULYIgg9vUN9s7GxPWn42NjQPQaHfJERmtsjcbgihhpJyRbN6F6vSpQumWpYpMxQAuWKdPFY6LEFtiDKN5Xmqtjqp36tI1nVUcFSGqxMhlRlUQv/9qvy9YvibRuvE78LfjYuxxicIlKsRYsXePXROjyqgJ4gnOnCGFqwHJRl+wfCOsHjoTFgpVlJgWtUCAM9VUe+JqKl06+9Q00EaLURHEE5w5w4VRB0wDQHKnL9j6V1jtyvY25S0cr8rUqJB1QTyh8lIXRh2S6QjqDMRy4ASSO72B1o3Z2KaGs+2a61WayzpZFSQQmOlzWbyJZDqw6+Q4bo1EWt7AEMuBLwXcQRa2qVwma4J4vZVBC6Me8AvY2XfSddOxcPgEQPRQyztScDPwBbA7G5FxhlxL6lm5D/H7r/ZLYdUCXmDXV+O5+Vik+avhYzrbw7s8nrnF3d2e/mz+DCHXkrpyQUKhipKYJd8CfMDuwvzYzdHwgROJxnZ37z6lev1cR6kgwyqmILA7z3X6pgMH2r5MOlEzhLIcMr20yjNUvgqxV5rGLW1tbcdV2c+UXCt7lUTI9NIqj9s0h8Qw++XS7u4WRzzyyLWy17YggUDFFMs03wBKgaZBMcJqjjOOQWwJEghUTLGErEUyE/jAHODGTMXIVh9jxd49ORUlGQsSClVdEZNmLZIy4INYv3njJ5/szzwystTHGBNl79D5J0EZ8GFcDCWn1g82VKc07sr5TSqWcxxpV1mlpdWXDzsZ2DyQN6BMDE0GggyY/f8AWSFgX54r/8Z/Hzz4WTYcW3pHazbMOp5M7kP6gPfdrvwlbW1Nx1Q7dIZo9+lsmXY0aeeQZL9wVYXPUzAayzgOR/XUh/Pm38outAsXBMcKMlZxrCA6qTuMsZrUHSvIWEUL4jC0IA7D9uP3XD9t7jTsR0iOnzZ3Gsp66nf9clNK4zY9cpeqJf8v0TnEYSgX5LXfPqDa5JhCuSAnPnf0u10cj3JBLr50qmqTYwrlgqxcu0G1yTGFTuoOQyd1h6GTusPQW5bD0II4DC2Iw3CsIKpOnaj6OYLj39s7EqpuDFWdOlF10DoX3tubEH1jaI+0336WYUNqrJJ2Iy79CEn/n6WMWUQKr7zVaDQajUaj0Wg0Go1Go9FoNBqNRqMZa/wXuO7Kv1MtJYEAAAAASUVORK5CYII=',
    deathCasesImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAMfklEQVR4nOWbfYwdV3XAf+e+j3kf6+/11vbuBuNdxw1pIcWKFNoKkRAKEiCVUgJtReAfhFSBWhSalBYRY6o2Rf1QVan9q6qIFBFa+vUHxlYgKWoh8eIAThy8zvoTOwTbxM7a2fc5M6d/vPfmzZ135+28t1tUqUeanZkz956ve8655953F/6fgyQR51RLpsUB4CMq7OjhtXdX+z357HpfDT9UIAc+9VlsXPd+VuCQFHh4VuTaUH5Lql6xzTeBX4kU7t1j0ie/JZ9d79Y3x0dJ05yMyjvoJAxxvlDgzl0iP43TNvGXos9DDFFeExcpz/F+rssFw9om+Q6TI9kvdt/tt/mbJF/L7hdauqQwn6a86z7wrCDXfkLhqX/CnH4Os3w1XevVQIRw03bC+TfRvvsDsG1H/5NDiYH7YEjcfF1RNlos4i/nW9pQ8HqKxJXLojiAOfcCpUe/AI3aauqNBuUqjfs/S7j79mFuvpoRlncXZXOcrBUCCpdgdeWdbg7QrOM9/hfrrzxAfQXvsS9CozZSeFjyCU8kyVoGQHksi/IkCPdw+ae/htx4ZU16DgN57TqFZw4NyJBR3mtBwB8laeatlyKPtFv8msJdvc6ZZ4MwpLBw2CZeKbD5ti0UN3uIGZLmHaCh0rre5NXF6/i1doTPHf06vPV9qMkBHddW7bi69t7t+40QDmvAA3srcinJx/KAWZF6ocg9wOcVTisELuVdrpb74VHk+pWIlhhhcv8k3lYPMWm90i8x4G3z2Pbm7ZbxzKtXyZ1ccGb/UKkpfE6Vubki+XlPZM6TTfOefNCl/IABekaY8+TAvCd793qS3+uJhEU2KrwPOJWWDwrPfM2iU9lVJVeyHGwsyJdzVHZVbNzTh1zTYM2E3HOrJ1+4tSRnRSTIQn/AAC74eZGb+zz596bHW4CLySQjly+SP/u81ac6PTHqoKde1ZkN1nyVP3Mc+ckFOwSFR26tyNEs+oxsgB68UeQ6wgM92cJuEvSOHrKSg7fFo7ChMKosqVCYKOBt9ixcceFwx+27shjlsXFoj2QAgFyRI9H0AtCsU/z+U1ab6uwE6zb83as6M2HxKHzvSbQ3JQJtD2eMrwYjG8CAxKe/wrPftOb9nGfwJsvrrD6UpkrkvFzER7qGjxJgk9lRdenqMxrU6rwzLpiXmPoq0xOd6msVjaS6gpk/h9l7HqmurG4BhMquqsXLO3oIVe2FwO+MqguMaICTN3Qbwl9Go790nNyVH0XfxUBlupraPw6y6yoUfMi3kZ1XVu8AVGaqVhlsrlwkf/a5Tj6CP3y+pr+cXZsujSyNllQ3Pl/T+/wCP1CY6bmdd9Se+kpTZXJFQyanzveLGwp+pj65oqE0VbZ4ev0psYzwjRfq+vDzDZ1T1RwZIA+w2NB9OeGRUHk7sCHJutkAxJ76zKtXKSwuWMSq03ai+t+AyuwE9cv16L2weBRz7Qq6dQqgrHDAKAdONuFkQ63FkQhnRTjUKvDw7d3NEbPY0H1GeFrh1+kqb4H2b/Hs7y18HQnDviAbihQ3FbNnNRefDJe3sUhxQzHqJmGId+yItS6JyxlnpcoeVT7htXj2lOokgMkJjwBbkjW+U97eg9+mdMxeWFVnssX+ekBl2q4MvYUjqN8aNEL3e9IQIew2rc7mSF7hHmIN4+AiqIB34tuYleWonckL5amSgwLIRK2f8IaAuf20jfDz6I+3ozcrA23LO8rcOHODsN3xQFO7QfHEd2jd8bY+365CKv0iUrt/uon0vdBNgs7RT+JixiictCvO8s4qknOv9mR6deWdkPeRnVfdNI1Q3mEbprj43UhBSw8GPaD7PYSOAZ7A3aDfMWGM/LXLVtvSVi97rI8KKXRLW+zSOHf98qDcCdkjVAf3BIDxfT4JvOTs7OCNQuiVHBTdl740Cf4Yq8J2Hv3xZCrdpGZh0RsYsDR9UK7ltLM5kn9DVV4++5rub+c5qPAO4PUoA8TiBINtu+DciQjv13y8bfaIRH1uVtDFWwbw5hfO2gqc2JNmCie0Y5sk9GSKyQmxBWQ3FwBXRTiSV/54riQ/gm4dsGdCLgMfz8r84h/8xoPAn/eFGSPG1wh+zV7ul44dfuDW3/7EX41KZ7wdi6B1Ir5AD2r+2uN9xP7Bim10CeXkOGzHMkCoumhiBmiv9ErZtcBo/ZNeF+RZHIfryKtBgFs23XkeiOrRsB1Gc/LPAhz86q+b2H9hHFpjGUAOHAiBpTjOXwkyl7NrKYVRV86RU12ZRoaxdy1FOanCG3vv7bpPYdP422CjBICfiP/6L/7qHQsr/6FCZ0SlW/0ZQAzXJeRRU+bB20VaSVqRAY6pFkpN/gTlfoUdIf3qz/W8/NSX2fjklyNCQa0NJOqDkSC7CYKEB/iTMxaVeOkryhaE39MmCnwqSSsKgYkmXxR4EPpnAoZBO8YUfrZTYbtu82pPzaS0jIHyYRc68oAQPjqKEP52m2nQywHjwgh9gxW7BvAnM2wHijvfxZEjie9vm7ZOI/iNAA3Xo/gfDqqK34wZQAQ/VgUOgX90IU3s4UsjCVIo4m+esnCd2MyaymP5t53L3C9YsYuuYMvPoYX+BokDXgb+7KrHQ66PkRSBx0PaBOB+YGsatd6GL3TCIH+9vzL06wH5iWwTS3hpK2bmGqCEl7Zl6tPjEYf29pnkYYmLGvKxDVX+c69IczV6kbTdKeJTODIlwPfrutsP+XvgXZEwk7Pw4rN9YWo+JXUvipKgy2WC5elMbePQXkmfAQDE8OE3l+VbWellLoR+qSznQ8PvQneDEWhvtxUI6iOEwJhX0HAYIHYKRBocz6oTjFgI5QQT9jhpx/3i0K5l+kHWAYpp+kjb7/zgOAwCu+DrTYG9MPDL3AmDJ0HSILMHLNR0loC/640+QJBwv2SBklmIRhtptCHQVZ2guZxYBE3aOSAX8g8/eE3fuaTZYjEP8EJdbzGGz6PcG8KMqwJMlvEChNVNhJWNmNoNADQEvx6SL422xJBWNsOpCKHf95CguomwujEKya4hZtVwuFaH5+oa4Y1QF3iGkM/eVpHv9GiYFxu6Jyc8I8pHgZnk8TNrq1NiuO6HZBLqlMQjxnbGZmEiwnqjH8nokC/2vQzcLYanTtX17ZEBEA4K7Ezo6STqMk6yDE1OU1lAi9kWUX5s9MGO/zT5opc+FBH+tvdigPfEv7qslzyPG+Fl0APGMUDo5VAvj/372+DlN+wEaMV/ykCl6HHbqZpOQycHxHUeWE1FxB3JWYFgu8MAI1fEQugVwOt4gvgBptYemBFaiTVAsH3GrfQqxoiDAZ5wHlROEHG5lDC4KOpsVq5trte8ISwPhkXzRmIG6BnAIWskqjhx39tXkZcATE75DBCdbsyidJxpmKjFw3ZI0NY1lzxhfjAc/GY/BLRQJNg8ZWk2YIzEwHbLl5oxfLKHM3MlWTLKXSL8i8ByvHGa0tYlYu3JAwRj5IEkiG9rHyaU8SdnECP2QCWNgTWIPwX+DeEt84X+NJgHmC/JaeA3xxX20u+/93EVPhgJV/MpbBj/jKC0Q3K1lr3qS9i0+PKZx/dX5LfGZtKFtZ9kBNTIYvzX1M5M4M6apuFjmgGjHqGPuz+A6njb4ElYHwMoi3EPTZsKTd3HNMYrl1sN22Ai4/0QMiDTuhBR3xoNv+7eoTat8XND86b9W6CG8n/IA6R6ChohXYMGjYDQDzHJMwO9o6UjgyRrgBDjLaW1HgXWxQNm//qf68CFOK5xZWALntDLjTUnNv0Bq53v8lwzpHrAmbre4ndXiAozxGSCwVVi+0sHKZw6FvW/eaGGopQmPXKFjieE5U7haRrZkqAKtHx45dzAf6CsS/zDYGUIwIsN3WOE/w5h57D/yNCYR3vf+iqlI4+ul1xDofGuj9B86/sHqryYMjVjWFDhc/sK8l/DaDlDQISDCjvj1VSSSVQYdYVo7b8XLQ0eaFpv0FIVf/+9GIfyvcsIFZS3mZBvvNjQdwyj5zYAvCdN0SSzyEgbNlO/79OQH7pFvTYoFKl/6NPoxKbUkj0xYEUj/Okwks4QONPUZYWN4HD9ISEBIFcu4j35FXKnj1tH6dYCOrEZf/5NtO6+D53q/wo04JVub63Pe5LqmmkG+Crw/jQlh/3/8ADO0XY1SK7xk4I6cemhenzOkzvSeLmnQeUzAq+44r7HzJWAnFe3rZFYv3j/BC7ezgyjmyJL4lLgYJryqQaYK8lSqNwldFaIWeJtVUPgUEjsb8nvabTjipP43r23gIWc8u45T/51mAH+B6tnkJV+3zWlAAAAAElFTkSuQmCC',
    recoveredCasesImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAF8UlEQVRoge1YaUxUVxg935sVBhgWBwQdYARURtoi1apxyQBjQNSmbTrYLWkaTVpr26SLAeraNDa2xm62SZvapBpbjZrQaG0tCgkGmqptQBatIjKtqHEEZO9s773+oEaIMO/eYWz6g/Nr3tzv3O+c9+53N2ACExgXKFQd5eXlOQAcHKXJB6AHQBuAc0RU6Xa7j9XW1vaFIm9IDCxevNik0WiaAZgYKf0A9qnV6vcqKiqujie3ajzkO0hPT/8GwFwOihbAHEmS1losFn9KSsppp9MpBZM76C9gt9sXSZL0JIAiABnB9vMvTni93lU1NTW3eYncBpYuXZohiuJOACt5uQGFENUDsFdWVnby8ASeYJvNNl8UxV8RYvEAIMtytizLRxwOh5aHx1wD+fn504moBoCRWx07zG6329jW1naclcD0BbZu3SpIkvQ1gIigpTFCluVXcnNzF7LGMxmorq5eQUSLgpfFBYGIPgJjfTIZIKJ145LEj7l2uz2fJVDRgN1uNwLIG6s9xkCYn6bm0MYGSZLWssQpGhBF0QZgTIVpJgEv52qgCcmSOAJFNptNseYUDRBRwBV2yQwVpsQIeMnGNfuxQE9EisOIpQasYzXERRDs1qGP81iOBk/P07DLYwARLVCKYTGQNnrnwPpCHXTqu5PFmiVavL1CD4OOb4Efa/jJsvyQEpel+hJH+3NdnhZzLfdmzs9UYUFaOCqafDjrFNHqkuD2ASoBmBRBMEUSEqMFpMQRppkEJMcJiNARfCJw6pKIncfd8PiH+iIii5K4gK/K4XCoOjs7fcPjDDrCGwU62GaEvmoB4MAZH76q9t557K6qqooJFK80hFQYJj4nRYUvnw+7b+IBYGH6iL4NSvEBDRw6dMg7/HlHsR6JxpAd4kaFKXKEJMVZgWs3+l9AzzmR/e8M8CKggbrG/TarNTmoo14oYLUmS3WN+22BYgIaGNQl7F1e+o5gTpkcUmEsSJoaj+Ulm4UBvXlfoLiABoy9lxJ8uhis2rgBkcZ7zzEXnT3Yvvsctu8+h4vOHmZxSrwIoxHPbNkEn34SonubJgVtwNr6iTu2uw7+KDNWb9sCv6Ab0V5+0omefh96+n0or/yT2UAgnihosfrdDfBHmRHT3YDMy7vcQRsgSTw5p6EUek8HYJqGs+lvQqLQ7neGQyIVfkt/HZQwHTpvF+Y0lkKQvCcCcQLPQiSU6bydnbObNoJkCR1Rs1BveREyDa0Fj9tTYYzUwhipxRP5KcxCR+cRGlLXwBX1IEiWMbt5E/QeVxdUUllAiUrJ5IPTp0DEhy0zS4r/mOoAAKS6KvDAX3uYBbPgvPlZtCYUAQBmXjuIjAvvH4ZKKqPi1suBeMzLqnT80Wtn0tcnuYxDG8RwjwsJPXVI6jqN2P5LAGRu0X1hU3E95hHciJmPvrApAABTbxPmtXzQLhR+b2bpg/ksSLJ0Pqft86RT1m0Y1JowqItHW3wB2uILoPX3ImbgCqL7L8M46ES41wW9rxsqcaj+RJUebk00BrXx6AlPRXdEOm4bpsGrjhqRI8zbiZwrn4FkqZlVF89htkbjH7A/3LoLtTM2QxLuUr3qKNw0ZuOmMZuju5EQZBE5rbug9fcBkGuYeRwpygEgeqAV1vbvuAUqwXp1H2IHWoYeiMqZVbEG0rIjDYD8CwBYXD/z6lOExVVx52cNFR5lHkKcmzkqQTDVyg4ZMjbyELgM0LKjNQC+5ZLEhz1UdLSah8C/ndbQWwCucfMUQe0Q1CW8LG4DZD9yE4JUKEhe5WBWEZIXkPwrqaDcxc0NJiEVHGuyuE6sV4t/B0MfAbXoRnJHVRkt/7E+KC3jSd7YfPi59tiFe3xqQ1AvQiP2S+aOUy9kZT21N1gN4z6ht9Tvnn0ratZPHZGzEnh4sX0Xb8V52pZlZhb/Pp78IbtiqLvww44uQ8ZrA2GJAS9JDe4bnriBlo+zZ64oDUXekN6RyDKoufnA2r5w86sD2smpHm20DgB03m6PwXPDGTF4/dOsrOIviO7rWjKBCfDgH5Ta3j3O+3BTAAAAAElFTkSuQmCC',
    activeCasesImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAJ+ElEQVR4nO2bXXBV1RXHf+uce5MASSCBlE/BqUAhJCCtrdanznRaawvtiKJVqxBUcMgNmhDb6RsPfbGGiCQBQ2iCOH4gjq3K2A+nM7x0xjKOOECCIXE6Fj9AAyHJDeR+7dWHk3u5Cbncc8q9lwddTyf7rLX+/7PuWnvvs84OfCNfb5FcAz5d0zbPp/4nQFeBLHRGTY+qHLJi1nN1z6/7LJd8chqAhuqOh0TYDUxJoTKsIpvqm9a/lCtOOQvA9kD7b0D2A7KgwKJyskWZzwLgy4hy/GKM/4YMgKrog/VNG17JBa+cBODpwJ/m+LC7QQt/UGizYoo9od7R4RjvB2OgDMWUxb/dVXUm29ysbAMA+LADoIULCqyUDw+wcorN/HwLhCKfUJMLbjkJALAaoHJyerjK0QCp8IvsUnIkRwHQRQBlvvQVl6SzKIuEEuLLBQigAJGLw87FVSSqAHkJm2xLrkqgC6Avlh6uzzg6Cj3ZpeRIrkrgFYDOcHq4zogzBwi8mV1OjuQkACbq2y/I4OmYxbFI6lXgWNjmdFQQuGCi9q5ccMvZRqihpv0eUTkAWDf4lHJfjBm2U+Z9MaErYnHaKRGjwtr6pqo3csErV5MgGOtLRBXgdFQ4HU0NLUYGckUrJxnQsLF1huTlfQjMRXWnWHylKncBN42q9IroX9RQhsgW4LN8tVYEWtadyza3rGeAotKYv28vylzgSGEkUr9pz6YI8Ifxutu2bfMVfbXgFoTbQ2L2K7pKkKwuh6lnpAxJUeDGGoFaYMBg/2TL7kdS/qqHDx82d3z/V+8isg5Y/t6tR/v+fuTNI9nkl9USeGbL3grL2EeASSr6gNs3vNEJ8yAQsgy31e6q+jBbHLO2DLZubJ1sGes1YBLQ5uX1tr5pw+uI7AXyjcVLrRtbJ2eLZ9YCEMzPfw5kqUBPgblY59W+MBR6AvQkUB7My9uRBYpAlgLQUNN+D6qPAiEx3Fu9qzro1cemPZsuGsvcC1wCHmuoab8/40TJQgC2b26/QVRaAUS0/lrq96mdj55Q+B2AqOx69smOGzNEMyEZDcC2bdt8iLwKlALv1DZVtVyrz63N65sR3gSmmSgHWje2+q+ZaJJ4XgX+uLljlm1pAOSXOBuZiMJ/BN5AdUZ8I6Ph8M31ezb1ZYJk8kZK4DlE+1RZA/JtwC9CrzHyllFt8dpG8xSA7YGOtUAbMPUqasbAj59qrjrsxXc6eSbQ8SML/snVs/aCij5W37Thdbd+Xe8ERx/+VcAys2cSrShHS0vAtpC+8/i6urE+dVr6lpLxZctSJsd/LjOzjGjlMkzZdMQocu48vs6TWF+cnSYqBxpq2u9zGwRXGbCzpr0sotILFEdXVBC9uXJCPft4J/4PjgH0h2N5N/1+94P9bvynk8bavaUasT8GpsUqy4msXDEhc9/RY/iOdQIMaDi80E0JupoEwxAAis2cWSkfHiBWuYzYvLkAJXl2ZLMb325EI3Y1MC02e1bKhweIrlyOmTMLYKrk5QXc+HYVAFFZBRAtX5JWN7YsrqN3uvHtUn6e8J0mZ5M4rnbj2OUcIItBMaUlaTV1ekKnPHl8e2Dfy4LeMhhk+bZ9VSNexuK+tKw0LX4Sx4UuHszb63CfiWAiV9exwhHmOZfRsXf0doUFhVNiM4FPvI05vr6KhDBp3o7tWJi5rp/I9UbI9ADk951Pq5l3LjHvfeyBRzrpBfD3p28U+RP4csqNY5cBkLcAik/2XLU9IQrF3b3xvw6Nu/0v4FRw2D77f4y9A1DUffWYikJx16n49dtXVY7buFHaWdNeFsHqRbV4YNkSBpcvRcdZikLxiZNMPf4RKnLOtnVh7Y6qC278p5PG2r2lGrU/Rpl2YWUFg0uv/GgkClOPd1F8ohuECxoKL3KzDLreCTbUdKwR5SBgjcyZyeCSRURKnA2hv3+Aoo96mfT5GUBiInpXXVOVq1/ArTRWv3CXEfO6gHXphrkMli8kVDoNgPy+fqZ2dlPw+VkAI2rdU9ey7s9u/HraCjfUdKxBaZcUW2EVOWcZ80hdy4asfNTYHth3t0KboBMvR8IFhUe8tNQ9vww1bu74mVr8FRgBGZ3p1QcUiOHOul1Vf/Pq83rie34dVps7AFS1ZWvz+qKtzeuLEJyvOJb81Ks/zzKKkSl8TwFQVFDWONccTLoVv16rV0yPmRNFRdGM4nsKQOOWjh8C84HT9S1ViXZ1XdP6fwOfKDpvR/X+27z49CKjvhdkEt9bCaisBUA4mPzBQhAV5A1HRdd68ukJXjOO7zoAyenP2PRzSDh9fMhSGSSnfybxXQcgOf1HU26MPNny8HtksQyS0z+T+O5LIEX6xyXbZZAq/a8V31UA0qV/gkSWymA0/e/OBr6rAIymVMr0j0tyGjbW7LvVje/rje8uA9KkX1yS0xDIWBlkE/+KhsizT74420RjNc5pbm4CVFEbQGK8m9aj0X9gUYuyeXugYxMgIvRiOBRVmtL17XONP6ZOGmra7xe19oAWpnDfr8KjqV42Rj9rtwHTJrRWhgQ21rVUvZrCPuf4iQA0BjoeUHgRsC7N9zO8NJ/wdBu1Ie+cobBzhEmfRABiKtw7nkTyIaiReX6CFQWEyiwkBv6+GIVdISZ9GgEwoA9sbd5wINn+euELQOPjL8xVn+kCige/W8BQRcGEASzqClH8/iUY13d/5vH937J8sR6geOB7kwguy5/QvvBEiKkfOPaWz7e0dsdDX2Qaf3BFPsHlkyZcAwqPjzD16MgYfGcS9JkAUHxpvj8lOMBQeT6X5vthXN9d/NHquH2qhwcIVuRzaZ5jb6KR6sSNDOGHZvsYWjHxwwMEKwsYGYdvASjOyezhpanJxyVJZ1WCgDo9+OASF/aJAEnCPmP45amDF5fgOPz4MrgYIFKa/sxUpCShc7nvrs7//kRnpLcPT0/ofCdpOCP44TLv+PFl0AIIXhzEpDnSbl3u9l9ej8W5Dl4cwqThYEUnHs4E/vDQELE0STAeP54BpwDy+9PvHgv647jSmzTcA1CQ/rNBwl6E5L59ZvBd9KDH41sAgtNDL+1Jfyax9FRcR5P7/ocASjzYq2qia5wp/NJe7/hOAHy+naDBwi+U0u7UTqZ/pEw5g9N9DYcTx1/8oi0KA27tVXXItmNN8fFM4U8+q8zo9IZ/eSNU3fFrFV4GJDhb6F8kjJQ4zgr6nchNOQOKGkvtK/ruTsvavCaI5djDSKnjvuA8lJ4yDnmndu/b2lw15q0uk/gXy4Tzi4VQCRhbx9iPx5exTtrvU6VNRIomDKFwQYy1IdVHh+2BfXcjuhedeCuqqkNYbEh1euN64F8x6+x4om1mLOarVlgtzlIRwZlk3jZRe/dTzz/85YTkkuyjxt48eqZgIeBX6BbHvtmN/fXE/0a+bvI/lHdJ1Rk451AAAAAASUVORK5CYII='
};

export const chartCOnfig = {
    lineChartOptions: {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        responsive: true,
        tooltips: {
            mode: 'index' as InteractionMode,
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Montserrat',
            bodyFontFamily: 'Montserrat',
            cornerRadius: 3,
            intersect: false,
        },
        scales: {
            xAxes: [{
                gridLines: {
                    color: 'transparent',
                    zeroLineColor: 'transparent'
                },
                ticks: {
                    fontSize: 2,
                    fontColor: 'transparent'
                }
            }
            ],
            yAxes: [{
                display: false,
                ticks: {
                    display: false
                }
            }
            ]
        },
        title: {
            display: false,
        },
        elements: {
            line: {
                tension: 0.00001,
                borderWidth: 0
            },
            point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4
            }
        }
    },
    lineChartType: 'line',
    lineChartData: [
        {
            data: null,
            type: 'line',
            label: 'Cases',
            backgroundColor: 'transparent',
            borderColor: 'rgba(255,255,255,.55)',
            pointBackgroundColor: 'rgba(255,255,255,.1)',
            pointBorderColor: 'rgba(255,255,255,.5)',
            pointHoverBackgroundColor: 'rgba(255,255,255,.1)',
            pointHoverBorderColor: 'rgba(255,255,255,.1)'
        }
    ]
};

export const flagData = [
    {
        alpha2Code: 'AF',
        alpha3Code: 'AFG',
        name: 'Afghanistan',
        flag: 'https://restcountries.eu/data/afg.svg'
    },
    {
        alpha2Code: 'AL',
        alpha3Code: 'ALB',
        name: 'Albania',
        flag: 'https://restcountries.eu/data/alb.svg'
    },
    {
        alpha2Code: 'DZ',
        alpha3Code: 'DZA',
        name: 'Algeria',
        flag: 'https://restcountries.eu/data/dza.svg'
    },
    {
        alpha2Code: 'AD',
        alpha3Code: 'AND',
        name: 'Andorra',
        flag: 'https://restcountries.eu/data/and.svg'
    },
    {
        alpha2Code: 'AO',
        alpha3Code: 'AGO',
        name: 'Angola',
        flag: 'https://restcountries.eu/data/ago.svg'
    },
    {
        alpha2Code: 'AI',
        alpha3Code: 'AIA',
        name: 'Anguilla',
        flag: 'https://restcountries.eu/data/aia.svg'
    },
    {
        alpha2Code: 'AQ',
        alpha3Code: 'ATA',
        name: 'Antarctica',
        flag: 'https://restcountries.eu/data/ata.svg'
    },
    {
        alpha2Code: 'AG',
        alpha3Code: 'ATG',
        name: 'Antigua and Barbuda',
        flag: 'https://restcountries.eu/data/atg.svg'
    },
    {
        alpha2Code: 'AR',
        alpha3Code: 'ARG',
        name: 'Argentina',
        flag: 'https://restcountries.eu/data/arg.svg'
    },
    {
        alpha2Code: 'AM',
        alpha3Code: 'ARM',
        name: 'Armenia',
        flag: 'https://restcountries.eu/data/arm.svg'
    },
    {
        alpha2Code: 'AW',
        alpha3Code: 'ABW',
        name: 'Aruba',
        flag: 'https://restcountries.eu/data/abw.svg'
    },
    {
        alpha2Code: 'AU',
        alpha3Code: 'AUS',
        name: 'Australia',
        flag: 'https://restcountries.eu/data/aus.svg'
    },
    {
        alpha2Code: 'AT',
        alpha3Code: 'AUT',
        name: 'Austria',
        flag: 'https://restcountries.eu/data/aut.svg'
    },
    {
        alpha2Code: 'AZ',
        alpha3Code: 'AZE',
        name: 'Azerbaijan',
        flag: 'https://restcountries.eu/data/aze.svg'
    },
    {
        alpha2Code: 'BS',
        alpha3Code: 'BHS',
        name: 'Bahamas',
        flag: 'https://restcountries.eu/data/bhs.svg'
    },
    {
        alpha2Code: 'BH',
        alpha3Code: 'BHR',
        name: 'Bahrain',
        flag: 'https://restcountries.eu/data/bhr.svg'
    },
    {
        alpha2Code: 'BD',
        alpha3Code: 'BGD',
        name: 'Bangladesh',
        flag: 'https://restcountries.eu/data/bgd.svg'
    },
    {
        alpha2Code: 'BB',
        alpha3Code: 'BRB',
        name: 'Barbados',
        flag: 'https://restcountries.eu/data/brb.svg'
    },
    {
        alpha2Code: 'BY',
        alpha3Code: 'BLR',
        name: 'Belarus',
        flag: 'https://restcountries.eu/data/blr.svg'
    },
    {
        alpha2Code: 'BE',
        alpha3Code: 'BEL',
        name: 'Belgium',
        flag: 'https://restcountries.eu/data/bel.svg'
    },
    {
        alpha2Code: 'BZ',
        alpha3Code: 'BLZ',
        name: 'Belize',
        flag: 'https://restcountries.eu/data/blz.svg'
    },
    {
        alpha2Code: 'BJ',
        alpha3Code: 'BEN',
        name: 'Benin',
        flag: 'https://restcountries.eu/data/ben.svg'
    },
    {
        alpha2Code: 'BM',
        alpha3Code: 'BMU',
        name: 'Bermuda',
        flag: 'https://restcountries.eu/data/bmu.svg'
    },
    {
        alpha2Code: 'BT',
        alpha3Code: 'BTN',
        name: 'Bhutan',
        flag: 'https://restcountries.eu/data/btn.svg'
    },
    {
        alpha2Code: 'BO',
        alpha3Code: 'BOL',
        name: 'Bolivia',
        flag: 'https://restcountries.eu/data/bol.svg'
    },
    {
        alpha2Code: 'BA',
        alpha3Code: 'BIH',
        name: 'Bosnia and Herzegovina',
        flag: 'https://restcountries.eu/data/bih.svg'
    },
    {
        alpha2Code: 'BW',
        alpha3Code: 'BWA',
        name: 'Botswana',
        flag: 'https://restcountries.eu/data/bwa.svg'
    },
    {
        alpha2Code: 'BV',
        alpha3Code: 'BVT',
        name: 'Bouvet Island',
        flag: 'https://restcountries.eu/data/bvt.svg'
    },
    {
        alpha2Code: 'BR',
        alpha3Code: 'BRA',
        name: 'Brazil',
        flag: 'https://restcountries.eu/data/bra.svg'
    },
    {
        alpha2Code: 'BN',
        alpha3Code: 'BRN',
        name: 'Brunei',
        flag: 'https://restcountries.eu/data/brn.svg'
    },
    {
        alpha2Code: 'BG',
        alpha3Code: 'BGR',
        name: 'Bulgaria',
        flag: 'https://restcountries.eu/data/bgr.svg'
    },
    {
        alpha2Code: 'BF',
        alpha3Code: 'BFA',
        name: 'Burkina Faso',
        flag: 'https://restcountries.eu/data/bfa.svg'
    },
    {
        alpha2Code: 'BI',
        alpha3Code: 'BDI',
        name: 'Burundi',
        flag: 'https://restcountries.eu/data/bdi.svg'
    },
    {
        alpha2Code: 'KH',
        alpha3Code: 'KHM',
        name: 'Cambodia',
        flag: 'https://restcountries.eu/data/khm.svg'
    },
    {
        alpha2Code: 'CM',
        alpha3Code: 'CMR',
        name: 'Cameroon',
        flag: 'https://restcountries.eu/data/cmr.svg'
    },
    {
        alpha2Code: 'CA',
        alpha3Code: 'CAN',
        name: 'Canada',
        flag: 'https://restcountries.eu/data/can.svg'
    },
    {
        alpha2Code: 'CV',
        alpha3Code: 'CPV',
        name: 'Cabo Verde',
        flag: 'https://restcountries.eu/data/cpv.svg'
    },
    {
        alpha2Code: 'KY',
        alpha3Code: 'CYM',
        name: 'Cayman Islands',
        flag: 'https://restcountries.eu/data/cym.svg'
    },
    {
        alpha2Code: 'CF',
        alpha3Code: 'CAF',
        name: 'Central African Republic',
        flag: 'https://restcountries.eu/data/caf.svg'
    },
    {
        alpha2Code: 'TD',
        alpha3Code: 'TCD',
        name: 'Chad',
        flag: 'https://restcountries.eu/data/tcd.svg'
    },
    {
        alpha2Code: 'CL',
        alpha3Code: 'CHL',
        name: 'Chile',
        flag: 'https://restcountries.eu/data/chl.svg'
    },
    {
        alpha2Code: 'CN',
        alpha3Code: 'CHN',
        name: 'China',
        flag: 'https://restcountries.eu/data/chn.svg'
    },
    {
        alpha2Code: 'CX',
        alpha3Code: 'CXR',
        name: 'Christmas Island',
        flag: 'https://restcountries.eu/data/cxr.svg'
    },
    {
        alpha2Code: 'CC',
        alpha3Code: 'CCK',
        name: 'Cocos (Keeling) Islands',
        flag: 'https://restcountries.eu/data/cck.svg'
    },
    {
        alpha2Code: 'CO',
        alpha3Code: 'COL',
        name: 'Colombia',
        flag: 'https://restcountries.eu/data/col.svg'
    },
    {
        alpha2Code: 'KM',
        alpha3Code: 'COM',
        name: 'Comoros',
        flag: 'https://restcountries.eu/data/com.svg'
    },
    {
        alpha2Code: 'CG',
        alpha3Code: 'COG',
        name: 'Congo',
        flag: 'https://restcountries.eu/data/cog.svg'
    },
    {
        alpha2Code: 'CD',
        alpha3Code: 'COD',
        name: 'Congo',
        flag: 'https://restcountries.eu/data/cod.svg'
    },
    {
        alpha2Code: 'CK',
        alpha3Code: 'COK',
        name: 'Cook Islands',
        flag: 'https://restcountries.eu/data/cok.svg'
    },
    {
        alpha2Code: 'CR',
        alpha3Code: 'CRI',
        name: 'Costa Rica',
        flag: 'https://restcountries.eu/data/cri.svg'
    },
    {
        alpha2Code: 'CI',
        alpha3Code: 'CIV',
        name: 'Ivory Coast',
        flag: 'https://restcountries.eu/data/civ.svg'
    },
    {
        alpha2Code: 'HR',
        alpha3Code: 'HRV',
        name: 'Croatia',
        flag: 'https://restcountries.eu/data/hrv.svg'
    },
    {
        alpha2Code: 'CU',
        alpha3Code: 'CUB',
        name: 'Cuba',
        flag: 'https://restcountries.eu/data/cub.svg'
    },
    {
        alpha2Code: 'CW',
        alpha3Code: 'CUW',
        name: 'Curaçao',
        flag: 'https://restcountries.eu/data/cuw.svg'
    },
    {
        alpha2Code: 'CY',
        alpha3Code: 'CYP',
        name: 'Cyprus',
        flag: 'https://restcountries.eu/data/cyp.svg'
    },
    {
        alpha2Code: 'CZ',
        alpha3Code: 'CZE',
        name: 'Czechia',
        flag: 'https://restcountries.eu/data/cze.svg'
    },
    {
        alpha2Code: 'DK',
        alpha3Code: 'DNK',
        name: 'Denmark',
        flag: 'https://restcountries.eu/data/dnk.svg'
    },
    {
        alpha2Code: 'DJ',
        alpha3Code: 'DJI',
        name: 'Djibouti',
        flag: 'https://restcountries.eu/data/dji.svg'
    },
    {
        alpha2Code: 'DM',
        alpha3Code: 'DMA',
        name: 'Dominica',
        flag: 'https://restcountries.eu/data/dma.svg'
    },
    {
        alpha2Code: 'DO',
        alpha3Code: 'DOM',
        name: 'Dominican Republic',
        flag: 'https://restcountries.eu/data/dom.svg'
    },
    {
        alpha2Code: 'EC',
        alpha3Code: 'ECU',
        name: 'Ecuador',
        flag: 'https://restcountries.eu/data/ecu.svg'
    },
    {
        alpha2Code: 'EG',
        alpha3Code: 'EGY',
        name: 'Egypt',
        flag: 'https://restcountries.eu/data/egy.svg'
    },
    {
        alpha2Code: 'SV',
        alpha3Code: 'SLV',
        name: 'El Salvador',
        flag: 'https://restcountries.eu/data/slv.svg'
    },
    {
        alpha2Code: 'GQ',
        alpha3Code: 'GNQ',
        name: 'Equatorial Guinea',
        flag: 'https://restcountries.eu/data/gnq.svg'
    },
    {
        alpha2Code: 'ER',
        alpha3Code: 'ERI',
        name: 'Eritrea',
        flag: 'https://restcountries.eu/data/eri.svg'
    },
    {
        alpha2Code: 'EE',
        alpha3Code: 'EST',
        name: 'Estonia',
        flag: 'https://restcountries.eu/data/est.svg'
    },
    {
        alpha2Code: 'ET',
        alpha3Code: 'ETH',
        name: 'Ethiopia',
        flag: 'https://restcountries.eu/data/eth.svg'
    },
    {
        alpha2Code: 'FK',
        alpha3Code: 'FLK',
        name: 'Falkland Islands (Malvinas)',
        flag: 'https://restcountries.eu/data/flk.svg'
    },
    {
        alpha2Code: 'FO',
        alpha3Code: 'FRO',
        name: 'Faroe Islands',
        flag: 'https://restcountries.eu/data/fro.svg'
    },
    {
        alpha2Code: 'FJ',
        alpha3Code: 'FJI',
        name: 'Fiji',
        flag: 'https://restcountries.eu/data/fji.svg'
    },
    {
        alpha2Code: 'FI',
        alpha3Code: 'FIN',
        name: 'Finland',
        flag: 'https://restcountries.eu/data/fin.svg'
    },
    {
        alpha2Code: 'FR',
        alpha3Code: 'FRA',
        name: 'France',
        flag: 'https://restcountries.eu/data/fra.svg'
    },
    {
        alpha2Code: 'GF',
        alpha3Code: 'GUF',
        name: 'French Guiana',
        flag: 'https://restcountries.eu/data/guf.svg'
    },
    {
        alpha2Code: 'PF',
        alpha3Code: 'PYF',
        name: 'French Polynesia',
        flag: 'https://restcountries.eu/data/pyf.svg'
    },
    {
        alpha2Code: 'TF',
        alpha3Code: 'ATF',
        name: 'French Southern Territories',
        flag: 'https://restcountries.eu/data/atf.svg'
    },
    {
        alpha2Code: 'GA',
        alpha3Code: 'GAB',
        name: 'Gabon',
        flag: 'https://restcountries.eu/data/gab.svg'
    },
    {
        alpha2Code: 'GM',
        alpha3Code: 'GMB',
        name: 'Gambia',
        flag: 'https://restcountries.eu/data/gmb.svg'
    },
    {
        alpha2Code: 'GE',
        alpha3Code: 'GEO',
        name: 'Georgia',
        flag: 'https://restcountries.eu/data/geo.svg'
    },
    {
        alpha2Code: 'DE',
        alpha3Code: 'DEU',
        name: 'Germany',
        flag: 'https://restcountries.eu/data/deu.svg'
    },
    {
        alpha2Code: 'GH',
        alpha3Code: 'GHA',
        name: 'Ghana',
        flag: 'https://restcountries.eu/data/gha.svg'
    },
    {
        alpha2Code: 'GI',
        alpha3Code: 'GIB',
        name: 'Gibraltar',
        flag: 'https://restcountries.eu/data/gib.svg'
    },
    {
        alpha2Code: 'GR',
        alpha3Code: 'GRC',
        name: 'Greece',
        flag: 'https://restcountries.eu/data/grc.svg'
    },
    {
        alpha2Code: 'GL',
        alpha3Code: 'GRL',
        name: 'Greenland',
        flag: 'https://restcountries.eu/data/grl.svg'
    },
    {
        alpha2Code: 'GD',
        alpha3Code: 'GRD',
        name: 'Grenada',
        flag: 'https://restcountries.eu/data/grd.svg'
    },
    {
        alpha2Code: 'GP',
        alpha3Code: 'GLP',
        name: 'Guadeloupe',
        flag: 'https://restcountries.eu/data/glp.svg'
    },
    {
        alpha2Code: 'GU',
        alpha3Code: 'GUM',
        name: 'Guam',
        flag: 'https://restcountries.eu/data/gum.svg'
    },
    {
        alpha2Code: 'GT',
        alpha3Code: 'GTM',
        name: 'Guatemala',
        flag: 'https://restcountries.eu/data/gtm.svg'
    },
    {
        alpha2Code: 'GG',
        alpha3Code: 'GGY',
        name: 'Guernsey',
        flag: 'https://restcountries.eu/data/ggy.svg'
    },
    {
        alpha2Code: 'GN',
        alpha3Code: 'GIN',
        name: 'Guinea',
        flag: 'https://restcountries.eu/data/gin.svg'
    },
    {
        alpha2Code: 'GW',
        alpha3Code: 'GNB',
        name: 'Guinea-Bissau',
        flag: 'https://restcountries.eu/data/gnb.svg'
    },
    {
        alpha2Code: 'GY',
        alpha3Code: 'GUY',
        name: 'Guyana',
        flag: 'https://restcountries.eu/data/guy.svg'
    },
    {
        alpha2Code: 'HT',
        alpha3Code: 'HTI',
        name: 'Haiti',
        flag: 'https://restcountries.eu/data/hti.svg'
    },
    {
        alpha2Code: 'HM',
        alpha3Code: 'HMD',
        name: 'Heard Island and McDonald Islands',
        flag: 'https://restcountries.eu/data/hmd.svg'
    },
    {
        alpha2Code: 'VA',
        alpha3Code: 'VAT',
        name: 'Holy See (Vatican City State)',
        flag: 'https://restcountries.eu/data/vat.svg'
    },
    {
        alpha2Code: 'HN',
        alpha3Code: 'HND',
        name: 'Honduras',
        flag: 'https://restcountries.eu/data/hnd.svg'
    },
    {
        alpha2Code: 'HK',
        alpha3Code: 'HKG',
        name: 'Hong Kong',
        flag: 'https://restcountries.eu/data/hkg.svg'
    },
    {
        alpha2Code: 'HU',
        alpha3Code: 'HUN',
        name: 'Hungary',
        flag: 'https://restcountries.eu/data/hun.svg'
    },
    {
        alpha2Code: 'IS',
        alpha3Code: 'ISL',
        name: 'Iceland',
        flag: 'https://restcountries.eu/data/isl.svg'
    },
    {
        alpha2Code: 'IN',
        alpha3Code: 'IND',
        name: 'India',
        flag: 'https://restcountries.eu/data/ind.svg'
    },
    {
        alpha2Code: 'ID',
        alpha3Code: 'IDN',
        name: 'Indonesia',
        flag: 'https://restcountries.eu/data/idn.svg'
    },
    {
        alpha2Code: 'IR',
        alpha3Code: 'IRN',
        name: 'Iran',
        flag: 'https://restcountries.eu/data/irn.svg'
    },
    {
        alpha2Code: 'IQ',
        alpha3Code: 'IRQ',
        name: 'Iraq',
        flag: 'https://restcountries.eu/data/irq.svg'
    },
    {
        alpha2Code: 'IE',
        alpha3Code: 'IRL',
        name: 'Ireland',
        flag: 'https://restcountries.eu/data/irl.svg'
    },
    {
        alpha2Code: 'IM',
        alpha3Code: 'IMN',
        name: 'Isle of Man',
        flag: 'https://restcountries.eu/data/imn.svg'
    },
    {
        alpha2Code: 'IL',
        alpha3Code: 'ISR',
        name: 'Israel',
        flag: 'https://restcountries.eu/data/isr.svg'
    },
    {
        alpha2Code: 'IT',
        alpha3Code: 'ITA',
        name: 'Italy',
        flag: 'https://restcountries.eu/data/ita.svg'
    },
    {
        alpha2Code: 'JM',
        alpha3Code: 'JAM',
        name: 'Jamaica',
        flag: 'https://restcountries.eu/data/jam.svg'
    },
    {
        alpha2Code: 'JP',
        alpha3Code: 'JPN',
        name: 'Japan',
        flag: 'https://restcountries.eu/data/jpn.svg'
    },
    {
        alpha2Code: 'JE',
        alpha3Code: 'JEY',
        name: 'Jersey',
        flag: 'https://restcountries.eu/data/jey.svg'
    },
    {
        alpha2Code: 'JO',
        alpha3Code: 'JOR',
        name: 'Jordan',
        flag: 'https://restcountries.eu/data/jor.svg'
    },
    {
        alpha2Code: 'KZ',
        alpha3Code: 'KAZ',
        name: 'Kazakhstan',
        flag: 'https://restcountries.eu/data/kaz.svg'
    },
    {
        alpha2Code: 'KE',
        alpha3Code: 'KEN',
        name: 'Kenya',
        flag: 'https://restcountries.eu/data/ken.svg'
    },
    {
        alpha2Code: 'KI',
        alpha3Code: 'KIR',
        name: 'Kiribati',
        flag: 'https://restcountries.eu/data/kir.svg'
    },
    {
        alpha2Code: 'KP',
        alpha3Code: 'PRK',
        name: 'N. Korea',
        flag: 'https://restcountries.eu/data/prk.svg'
    },
    {
        alpha2Code: 'KR',
        alpha3Code: 'KOR',
        name: 'S. Korea',
        flag: 'https://restcountries.eu/data/kor.svg'
    },
    {
        alpha2Code: 'KW',
        alpha3Code: 'KWT',
        name: 'Kuwait',
        flag: 'https://restcountries.eu/data/kwt.svg'
    },
    {
        alpha2Code: 'KG',
        alpha3Code: 'KGZ',
        name: 'Kyrgyzstan',
        flag: 'https://restcountries.eu/data/kgz.svg'
    },
    {
        alpha2Code: 'LA',
        alpha3Code: 'LAO',
        name: 'Laos',
        flag: 'https://restcountries.eu/data/lao.svg'
    },
    {
        alpha2Code: 'LV',
        alpha3Code: 'LVA',
        name: 'Latvia',
        flag: 'https://restcountries.eu/data/lva.svg'
    },
    {
        alpha2Code: 'LB',
        alpha3Code: 'LBN',
        name: 'Lebanon',
        flag: 'https://restcountries.eu/data/lbn.svg'
    },
    {
        alpha2Code: 'LS',
        alpha3Code: 'LSO',
        name: 'Lesotho',
        flag: 'https://restcountries.eu/data/lso.svg'
    },
    {
        alpha2Code: 'LR',
        alpha3Code: 'LBR',
        name: 'Liberia',
        flag: 'https://restcountries.eu/data/lbr.svg'
    },
    {
        alpha2Code: 'LY',
        alpha3Code: 'LBY',
        name: 'Libya',
        flag: 'https://restcountries.eu/data/lby.svg'
    },
    {
        alpha2Code: 'LI',
        alpha3Code: 'LIE',
        name: 'Liechtenstein',
        flag: 'https://restcountries.eu/data/lie.svg'
    },
    {
        alpha2Code: 'LT',
        alpha3Code: 'LTU',
        name: 'Lithuania',
        flag: 'https://restcountries.eu/data/ltu.svg'
    },
    {
        alpha2Code: 'LU',
        alpha3Code: 'LUX',
        name: 'Luxembourg',
        flag: 'https://restcountries.eu/data/lux.svg'
    },
    {
        alpha2Code: 'MO',
        alpha3Code: 'MAC',
        name: 'Macao',
        flag: 'https://restcountries.eu/data/mac.svg'
    },
    {
        alpha2Code: 'MK',
        alpha3Code: 'MKD',
        name: 'North Macedonia',
        flag: 'https://restcountries.eu/data/mkd.svg'
    },
    {
        alpha2Code: 'MG',
        alpha3Code: 'MDG',
        name: 'Madagascar',
        flag: 'https://restcountries.eu/data/mdg.svg'
    },
    {
        alpha2Code: 'MW',
        alpha3Code: 'MWI',
        name: 'Malawi',
        flag: 'https://restcountries.eu/data/mwi.svg'
    },
    {
        alpha2Code: 'MY',
        alpha3Code: 'MYS',
        name: 'Malaysia',
        flag: 'https://restcountries.eu/data/mys.svg'
    },
    {
        alpha2Code: 'MV',
        alpha3Code: 'MDV',
        name: 'Maldives',
        flag: 'https://restcountries.eu/data/mdv.svg'
    },
    {
        alpha2Code: 'ML',
        alpha3Code: 'MLI',
        name: 'Mali',
        flag: 'https://restcountries.eu/data/mli.svg'
    },
    {
        alpha2Code: 'MT',
        alpha3Code: 'MLT',
        name: 'Malta',
        flag: 'https://restcountries.eu/data/mlt.svg'
    },
    {
        alpha2Code: 'MH',
        alpha3Code: 'MHL',
        name: 'Marshall Islands',
        flag: 'https://restcountries.eu/data/mhl.svg'
    },
    {
        alpha2Code: 'MQ',
        alpha3Code: 'MTQ',
        name: 'Martinique',
        flag: 'https://restcountries.eu/data/mtq.svg'
    },
    {
        alpha2Code: 'MR',
        alpha3Code: 'MRT',
        name: 'Mauritania',
        flag: 'https://restcountries.eu/data/mrt.svg'
    },
    {
        alpha2Code: 'MU',
        alpha3Code: 'MUS',
        name: 'Mauritius',
        flag: 'https://restcountries.eu/data/mus.svg'
    },
    {
        alpha2Code: 'YT',
        alpha3Code: 'MYT',
        name: 'Mayotte',
        flag: 'https://restcountries.eu/data/myt.svg'
    },
    {
        alpha2Code: 'MX',
        alpha3Code: 'MEX',
        name: 'Mexico',
        flag: 'https://restcountries.eu/data/mex.svg'
    },
    {
        alpha2Code: 'FM',
        alpha3Code: 'FSM',
        name: 'Micronesia (Federated States of)',
        flag: 'https://restcountries.eu/data/fsm.svg'
    },
    {
        alpha2Code: 'MD',
        alpha3Code: 'MDA',
        name: 'Moldova (Republic of)',
        flag: 'https://restcountries.eu/data/mda.svg'
    },
    {
        alpha2Code: 'MC',
        alpha3Code: 'MCO',
        name: 'Monaco',
        flag: 'https://restcountries.eu/data/mco.svg'
    },
    {
        alpha2Code: 'MN',
        alpha3Code: 'MNG',
        name: 'Mongolia',
        flag: 'https://restcountries.eu/data/mng.svg'
    },
    {
        alpha2Code: 'ME',
        alpha3Code: 'MNE',
        name: 'Montenegro',
        flag: 'https://restcountries.eu/data/mne.svg'
    },
    {
        alpha2Code: 'MS',
        alpha3Code: 'MSR',
        name: 'Montserrat',
        flag: 'https://restcountries.eu/data/msr.svg'
    },
    {
        alpha2Code: 'MA',
        alpha3Code: 'MAR',
        name: 'Morocco',
        flag: 'https://restcountries.eu/data/mar.svg'
    },
    {
        alpha2Code: 'MZ',
        alpha3Code: 'MOZ',
        name: 'Mozambique',
        flag: 'https://restcountries.eu/data/moz.svg'
    },
    {
        alpha2Code: 'MM',
        alpha3Code: 'MMR',
        name: 'Myanmar',
        flag: 'https://restcountries.eu/data/mmr.svg'
    },
    {
        alpha2Code: 'NA',
        alpha3Code: 'NAM',
        name: 'Namibia',
        flag: 'https://restcountries.eu/data/nam.svg'
    },
    {
        alpha2Code: 'NR',
        alpha3Code: 'NRU',
        name: 'Nauru',
        flag: 'https://restcountries.eu/data/nru.svg'
    },
    {
        alpha2Code: 'NP',
        alpha3Code: 'NPL',
        name: 'Nepal',
        flag: 'https://restcountries.eu/data/npl.svg'
    },
    {
        alpha2Code: 'NL',
        alpha3Code: 'NLD',
        name: 'Netherlands',
        flag: 'https://restcountries.eu/data/nld.svg'
    },
    {
        alpha2Code: 'NC',
        alpha3Code: 'NCL',
        name: 'New Caledonia',
        flag: 'https://restcountries.eu/data/ncl.svg'
    },
    {
        alpha2Code: 'NZ',
        alpha3Code: 'NZL',
        name: 'New Zealand',
        flag: 'https://restcountries.eu/data/nzl.svg'
    },
    {
        alpha2Code: 'NI',
        alpha3Code: 'NIC',
        name: 'Nicaragua',
        flag: 'https://restcountries.eu/data/nic.svg'
    },
    {
        alpha2Code: 'NE',
        alpha3Code: 'NER',
        name: 'Niger',
        flag: 'https://restcountries.eu/data/ner.svg'
    },
    {
        alpha2Code: 'NG',
        alpha3Code: 'NGA',
        name: 'Nigeria',
        flag: 'https://restcountries.eu/data/nga.svg'
    },
    {
        alpha2Code: 'NU',
        alpha3Code: 'NIU',
        name: 'Niue',
        flag: 'https://restcountries.eu/data/niu.svg'
    },
    {
        alpha2Code: 'NF',
        alpha3Code: 'NFK',
        name: 'Norfolk Island',
        flag: 'https://restcountries.eu/data/nfk.svg'
    },
    {
        alpha2Code: 'MP',
        alpha3Code: 'MNP',
        name: 'Northern Mariana Islands',
        flag: 'https://restcountries.eu/data/mnp.svg'
    },
    {
        alpha2Code: 'NO',
        alpha3Code: 'NOR',
        name: 'Norway',
        flag: 'https://restcountries.eu/data/nor.svg'
    },
    {
        alpha2Code: 'OM',
        alpha3Code: 'OMN',
        name: 'Oman',
        flag: 'https://restcountries.eu/data/omn.svg'
    },
    {
        alpha2Code: 'PK',
        alpha3Code: 'PAK',
        name: 'Pakistan',
        flag: 'https://restcountries.eu/data/pak.svg'
    },
    {
        alpha2Code: 'PW',
        alpha3Code: 'PLW',
        name: 'Palau',
        flag: 'https://restcountries.eu/data/plw.svg'
    },
    {
        alpha2Code: 'PS',
        alpha3Code: 'PSE',
        name: 'Palestine',
        flag: 'https://restcountries.eu/data/pse.svg'
    },
    {
        alpha2Code: 'PA',
        alpha3Code: 'PAN',
        name: 'Panama',
        flag: 'https://restcountries.eu/data/pan.svg'
    },
    {
        alpha2Code: 'PG',
        alpha3Code: 'PNG',
        name: 'Papua New Guinea',
        flag: 'https://restcountries.eu/data/png.svg'
    },
    {
        alpha2Code: 'PY',
        alpha3Code: 'PRY',
        name: 'Paraguay',
        flag: 'https://restcountries.eu/data/pry.svg'
    },
    {
        alpha2Code: 'PE',
        alpha3Code: 'PER',
        name: 'Peru',
        flag: 'https://restcountries.eu/data/per.svg'
    },
    {
        alpha2Code: 'PH',
        alpha3Code: 'PHL',
        name: 'Philippines',
        flag: 'https://restcountries.eu/data/phl.svg'
    },
    {
        alpha2Code: 'PN',
        alpha3Code: 'PCN',
        name: 'Pitcairn',
        flag: 'https://restcountries.eu/data/pcn.svg'
    },
    {
        alpha2Code: 'PL',
        alpha3Code: 'POL',
        name: 'Poland',
        flag: 'https://restcountries.eu/data/pol.svg'
    },
    {
        alpha2Code: 'PT',
        alpha3Code: 'PRT',
        name: 'Portugal',
        flag: 'https://restcountries.eu/data/prt.svg'
    },
    {
        alpha2Code: 'PR',
        alpha3Code: 'PRI',
        name: 'Puerto Rico',
        flag: 'https://restcountries.eu/data/pri.svg'
    },
    {
        alpha2Code: 'QA',
        alpha3Code: 'QAT',
        name: 'Qatar',
        flag: 'https://restcountries.eu/data/qat.svg'
    },
    {
        alpha2Code: 'RE',
        alpha3Code: 'REU',
        name: 'Réunion',
        flag: 'https://restcountries.eu/data/reu.svg'
    },
    {
        alpha2Code: 'RO',
        alpha3Code: 'ROU',
        name: 'Romania',
        flag: 'https://restcountries.eu/data/rou.svg'
    },
    {
        alpha2Code: 'RU',
        alpha3Code: 'RUS',
        name: 'Russia',
        flag: 'https://restcountries.eu/data/rus.svg'
    },
    {
        alpha2Code: 'RW',
        alpha3Code: 'RWA',
        name: 'Rwanda',
        flag: 'https://restcountries.eu/data/rwa.svg'
    },
    {
        alpha2Code: 'BL',
        alpha3Code: 'BLM',
        name: 'Saint Barthélemy',
        flag: 'https://restcountries.eu/data/blm.svg'
    },
    {
        alpha2Code: 'SH',
        alpha3Code: 'SHN',
        name: 'Saint Helena, Ascension and Tristan da Cunha',
        flag: 'https://restcountries.eu/data/shn.svg'
    },
    {
        alpha2Code: 'KN',
        alpha3Code: 'KNA',
        name: 'Saint Kitts and Nevis',
        flag: 'https://restcountries.eu/data/kna.svg'
    },
    {
        alpha2Code: 'LC',
        alpha3Code: 'LCA',
        name: 'Saint Lucia',
        flag: 'https://restcountries.eu/data/lca.svg'
    },
    {
        alpha2Code: 'MF',
        alpha3Code: 'MAF',
        name: 'Saint Martin',
        flag: 'https://restcountries.eu/data/maf.svg'
    },
    {
        alpha2Code: 'PM',
        alpha3Code: 'SPM',
        name: 'Saint Pierre and Miquelon',
        flag: 'https://restcountries.eu/data/spm.svg'
    },
    {
        alpha2Code: 'VC',
        alpha3Code: 'VCT',
        name: 'Saint Vincent and the Grenadines',
        flag: 'https://restcountries.eu/data/vct.svg'
    },
    {
        alpha2Code: 'WS',
        alpha3Code: 'WSM',
        name: 'Samoa',
        flag: 'https://restcountries.eu/data/wsm.svg'
    },
    {
        alpha2Code: 'SM',
        alpha3Code: 'SMR',
        name: 'San Marino',
        flag: 'https://restcountries.eu/data/smr.svg'
    },
    {
        alpha2Code: 'ST',
        alpha3Code: 'STP',
        name: 'Sao Tome and Principe',
        flag: 'https://restcountries.eu/data/stp.svg'
    },
    {
        alpha2Code: 'SA',
        alpha3Code: 'SAU',
        name: 'Saudi Arabia',
        flag: 'https://restcountries.eu/data/sau.svg'
    },
    {
        alpha2Code: 'SN',
        alpha3Code: 'SEN',
        name: 'Senegal',
        flag: 'https://restcountries.eu/data/sen.svg'
    },
    {
        alpha2Code: 'RS',
        alpha3Code: 'SRB',
        name: 'Serbia',
        flag: 'https://restcountries.eu/data/srb.svg'
    },
    {
        alpha2Code: 'SC',
        alpha3Code: 'SYC',
        name: 'Seychelles',
        flag: 'https://restcountries.eu/data/syc.svg'
    },
    {
        alpha2Code: 'SL',
        alpha3Code: 'SLE',
        name: 'Sierra Leone',
        flag: 'https://restcountries.eu/data/sle.svg'
    },
    {
        alpha2Code: 'SG',
        alpha3Code: 'SGP',
        name: 'Singapore',
        flag: 'https://restcountries.eu/data/sgp.svg'
    },
    {
        alpha2Code: 'SX',
        alpha3Code: 'SXM',
        name: 'Sint Maarten',
        flag: 'https://restcountries.eu/data/sxm.svg'
    },
    {
        alpha2Code: 'SK',
        alpha3Code: 'SVK',
        name: 'Slovakia',
        flag: 'https://restcountries.eu/data/svk.svg'
    },
    {
        alpha2Code: 'SI',
        alpha3Code: 'SVN',
        name: 'Slovenia',
        flag: 'https://restcountries.eu/data/svn.svg'
    },
    {
        alpha2Code: 'SB',
        alpha3Code: 'SLB',
        name: 'Solomon Islands',
        flag: 'https://restcountries.eu/data/slb.svg'
    },
    {
        alpha2Code: 'SO',
        alpha3Code: 'SOM',
        name: 'Somalia',
        flag: 'https://restcountries.eu/data/som.svg'
    },
    {
        alpha2Code: 'ZA',
        alpha3Code: 'ZAF',
        name: 'South Africa',
        flag: 'https://restcountries.eu/data/zaf.svg'
    },
    {
        alpha2Code: 'GS',
        alpha3Code: 'SGS',
        name: 'Georgia',
        flag: 'https://restcountries.eu/data/sgs.svg'
    },
    {
        alpha2Code: 'SS',
        alpha3Code: 'SSD',
        name: 'Sudan',
        flag: 'https://restcountries.eu/data/ssd.svg'
    },
    {
        alpha2Code: 'ES',
        alpha3Code: 'ESP',
        name: 'Spain',
        flag: 'https://restcountries.eu/data/esp.svg'
    },
    {
        alpha2Code: 'LK',
        alpha3Code: 'LKA',
        name: 'Sri Lanka',
        flag: 'https://restcountries.eu/data/lka.svg'
    },
    {
        alpha2Code: 'SD',
        alpha3Code: 'SDN',
        name: 'Sudan',
        flag: 'https://restcountries.eu/data/sdn.svg'
    },
    {
        alpha2Code: 'SR',
        alpha3Code: 'SUR',
        name: 'Suriname',
        flag: 'https://restcountries.eu/data/sur.svg'
    },
    {
        alpha2Code: 'SJ',
        alpha3Code: 'SJM',
        name: 'Svalbard and Jan Mayen',
        flag: 'https://restcountries.eu/data/sjm.svg'
    },
    {
        alpha2Code: 'SZ',
        alpha3Code: 'SWZ',
        name: 'Swaziland',
        flag: 'https://restcountries.eu/data/swz.svg'
    },
    {
        alpha2Code: 'SE',
        alpha3Code: 'SWE',
        name: 'Sweden',
        flag: 'https://restcountries.eu/data/swe.svg'
    },
    {
        alpha2Code: 'CH',
        alpha3Code: 'CHE',
        name: 'Switzerland',
        flag: 'https://restcountries.eu/data/che.svg'
    },
    {
        alpha2Code: 'SY',
        alpha3Code: 'SYR',
        name: 'Syrian',
        flag: 'https://restcountries.eu/data/syr.svg'
    },
    {
        alpha2Code: 'TW',
        alpha3Code: 'TWN',
        name: 'Taiwan',
        flag: 'https://restcountries.eu/data/twn.svg'
    },
    {
        alpha2Code: 'TJ',
        alpha3Code: 'TJK',
        name: 'Tajikistan',
        flag: 'https://restcountries.eu/data/tjk.svg'
    },
    {
        alpha2Code: 'TZ',
        alpha3Code: 'TZA',
        name: 'Tanzania',
        flag: 'https://restcountries.eu/data/tza.svg'
    },
    {
        alpha2Code: 'TH',
        alpha3Code: 'THA',
        name: 'Thailand',
        flag: 'https://restcountries.eu/data/tha.svg'
    },
    {
        alpha2Code: 'TL',
        alpha3Code: 'TLS',
        name: 'Timor-Leste',
        flag: 'https://restcountries.eu/data/tls.svg'
    },
    {
        alpha2Code: 'TG',
        alpha3Code: 'TGO',
        name: 'Togo',
        flag: 'https://restcountries.eu/data/tgo.svg'
    },
    {
        alpha2Code: 'TK',
        alpha3Code: 'TKL',
        name: 'Tokelau',
        flag: 'https://restcountries.eu/data/tkl.svg'
    },
    {
        alpha2Code: 'TO',
        alpha3Code: 'TON',
        name: 'Tonga',
        flag: 'https://restcountries.eu/data/ton.svg'
    },
    {
        alpha2Code: 'TT',
        alpha3Code: 'TTO',
        name: 'Trinidad and Tobago',
        flag: 'https://restcountries.eu/data/tto.svg'
    },
    {
        alpha2Code: 'TN',
        alpha3Code: 'TUN',
        name: 'Tunisia',
        flag: 'https://restcountries.eu/data/tun.svg'
    },
    {
        alpha2Code: 'TR',
        alpha3Code: 'TUR',
        name: 'Turkey',
        flag: 'https://restcountries.eu/data/tur.svg'
    },
    {
        alpha2Code: 'TM',
        alpha3Code: 'TKM',
        name: 'Turkmenistan',
        flag: 'https://restcountries.eu/data/tkm.svg'
    },
    {
        alpha2Code: 'TC',
        alpha3Code: 'TCA',
        name: 'Turks and Caicos',
        flag: 'https://restcountries.eu/data/tca.svg'
    },
    {
        alpha2Code: 'TV',
        alpha3Code: 'TUV',
        name: 'Tuvalu',
        flag: 'https://restcountries.eu/data/tuv.svg'
    },
    {
        alpha2Code: 'UG',
        alpha3Code: 'UGA',
        name: 'Uganda',
        flag: 'https://restcountries.eu/data/uga.svg'
    },
    {
        alpha2Code: 'UA',
        alpha3Code: 'UKR',
        name: 'Ukraine',
        flag: 'https://restcountries.eu/data/ukr.svg'
    },
    {
        alpha2Code: 'AE',
        alpha3Code: 'ARE',
        name: 'UAE',
        flag: 'https://restcountries.eu/data/are.svg'
    },
    {
        alpha2Code: 'GB',
        alpha3Code: 'GBR',
        name: 'UK',
        flag: 'https://restcountries.eu/data/gbr.svg'
    },
    {
        alpha2Code: 'US',
        alpha3Code: 'USA',
        name: 'USA',
        flag: 'https://restcountries.eu/data/usa.svg'
    },
    {
        alpha2Code: 'UM',
        alpha3Code: 'UMI',
        name: 'United States Minor Outlying Islands',
        flag: 'https://restcountries.eu/data/umi.svg'
    },
    {
        alpha2Code: 'UY',
        alpha3Code: 'URY',
        name: 'Uruguay',
        flag: 'https://restcountries.eu/data/ury.svg'
    },
    {
        alpha2Code: 'UZ',
        alpha3Code: 'UZB',
        name: 'Uzbekistan',
        flag: 'https://restcountries.eu/data/uzb.svg'
    },
    {
        alpha2Code: 'VU',
        alpha3Code: 'VUT',
        name: 'Vanuatu',
        flag: 'https://restcountries.eu/data/vut.svg'
    },
    {
        alpha2Code: 'VE',
        alpha3Code: 'VEN',
        name: 'Venezuela',
        flag: 'https://restcountries.eu/data/ven.svg'
    },
    {
        alpha2Code: 'VN',
        alpha3Code: 'VNM',
        name: 'Vietnam',
        flag: 'https://restcountries.eu/data/vnm.svg'
    },
    {
        alpha2Code: 'VI',
        alpha3Code: 'VIR',
        name: 'U.S Virgin Islands',
        flag: 'https://restcountries.eu/data/vir.svg'
    },
    {
        alpha2Code: 'WF',
        alpha3Code: 'WLF',
        name: 'Wallis and Futuna',
        flag: 'https://restcountries.eu/data/wlf.svg'
    },
    {
        alpha2Code: 'EH',
        alpha3Code: 'ESH',
        name: 'Western Sahara',
        flag: 'https://restcountries.eu/data/esh.svg'
    },
    {
        alpha2Code: 'YE',
        alpha3Code: 'YEM',
        name: 'Yemen',
        flag: 'https://restcountries.eu/data/yem.svg'
    },
    {
        alpha2Code: 'ZM',
        alpha3Code: 'ZMB',
        name: 'Zambia',
        flag: 'https://restcountries.eu/data/zmb.svg'
    },
    {
        alpha2Code: 'ZW',
        alpha3Code: 'ZWE',
        name: 'Zimbabwe',
        flag: 'https://restcountries.eu/data/zwe.svg'
    },
    {
        alpha2Code: 'VG',
        alpha3Code: 'VGB',
        name: 'British Virgin Islands',
        flag: 'https://restcountries.eu/data/vgb.svg'
    }
];
