import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class ErrorGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        const navigationExtraData = this.router.getCurrentNavigation().extras;
        const hasActive = Object.prototype.hasOwnProperty.call(navigationExtraData, 'state');
        // const navigationStart = this.router.events instanceof NavigationStart;
        if (hasActive) {
            if (navigationExtraData.state.active/*this.router.url === '/dashboard' && navigationStart*/) {
                return true;
            }
        }
        this.router.navigate(['/']);
    }

}
